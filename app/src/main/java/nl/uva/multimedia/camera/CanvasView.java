package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class CanvasView extends View {
    private int[] m_histogram_values = null;
    private int m_average = -1;
    private int m_median = -1;
    private double m_standard_deviation;
    private int m_bin_num = 0;

    protected Bitmap m_image = null;
    private int m_color;

    public CanvasView(Context context) {
        super(context);
    }

    public CanvasView(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    public CanvasView(Context context, AttributeSet attributes, int style) {
        super(context, attributes, style);
    }

    /* Called whenever the canvas is dirty and needs redrawing. */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

		/* Define the basic paint. */
        Paint paint = new Paint();

        if (m_histogram_values != null) {
            int length = m_histogram_values.length,
                width = getWidth(),
                height = getHeight(),
                max = 0;

            for (int value : m_histogram_values)
                if (max < value)
                    max = value;

            for (int i = 0; i < length; i++) {
                switch (m_color) {
                    case Color.RED:
                        paint.setColor(Color.rgb(255 / 10 * i, 0, 0));
                        break;
                    case Color.GREEN:
                        paint.setColor(Color.rgb(0, 255 / 10 * i, 0));
                        break;
                    case Color.BLUE:
                        paint.setColor(Color.rgb(0, 0, 255 / 10 * i));
                        break;
                    default:
                        paint.setColor(Color.BLACK);
                        break;
                }
                float left = ((float)i) / length * width,
                      top = height - ((float)m_histogram_values[i] / max) * (height - 120),
                      right =  (i + 1.0f) / length * width,
                      bot = height;
                canvas.drawRect(left, top, right, bot, paint);
            }
        }
	
		/* text inherits from the basic paint and adds text properties. */
        Paint text = new Paint(paint);
        text.setColor(Color.WHITE);
        text.setShadowLayer(3.0F, 3.0F, 3.0F, Color.rgb(0x20, 0x20, 0x20));
        text.setTextSize(30.0f);
	
		/* Save state */
        if (m_average >= 0 && m_median >= 0) {
            canvas.save();

            canvas.drawText(getResources().getString(R.string.average) + ": " + m_average, 30.0F, 30.0F, text);
            canvas.drawText(getResources().getString(R.string.median) + ": " + m_median, 30.0F, 60.0F, text);
            canvas.drawText(getResources().getString(R.string.standard_deviation) + ": " + m_standard_deviation, 30.0F, 90.0F, text);

            canvas.restore();
        }

		/* Paint a image if we have it, just a demo, clean this up so it works
		 * your way, or remove it if you don't need it
		 */
        if (m_image != null) {
            Rect rect = new Rect(
                    (int) (getWidth() * 0.25F), (int) (getHeight() * 0.25F),
                    (int) (getWidth() * 0.75F), (int) (getHeight() * 0.75F)
            );
            Paint bmp = new Paint();
            bmp.setAntiAlias(true);
            bmp.setFilterBitmap(true);
            bmp.setDither(true);

            canvas.drawBitmap(m_image, null, rect, paint);
        }
    }

    /* Accessors */
    public Bitmap getSelectedImage() {
        return m_image;
    }
    public int getBinNum() { return m_bin_num; }

    public void setSelectedImage(Bitmap image) {
        m_image = image;
    }
    public void setColor(int color) { m_color = color; }
    public void setBinNum(int binNum) { m_bin_num = binNum; }
    public void setHistogramValues(int[] histogramValues) { m_histogram_values = histogramValues; }
    public void setAverage(int average) { m_average = average; }
    public void setMedian(int median) { m_median = median; }
    public void setStandardDeviation(double standard_deviation) { m_standard_deviation = standard_deviation; }
}