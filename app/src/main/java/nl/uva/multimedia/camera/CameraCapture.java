package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */


/* Handles the camera data 
 *
 * The size is based on the size of the onscreen preview and calculated in
 * CameraView. This means that smaller screen devices, will, yes, have less
 * to calculate on.
 */

import android.graphics.Color;
import android.hardware.Camera;
import android.os.AsyncTask;

import java.util.Arrays;

class CameraCapture implements CameraView.PreviewCallback {
    private CanvasView m_canvas_view = null;
    private int m_image_width = 0;
    private int m_image_height = 0;
    private int m_color;
    private AsyncTask m_task = null;

    /* Is called by Android when a frame is ready */
    public void onPreviewFrame(byte[] data, Camera camera, boolean rotated) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPreviewSize();

		/* Rotated is true if the height and width parameters should be swapped
         * due to the image having been rotated internally. (Turns out it is
		 * rather tricky to patch a method in Java....) 
		 */
        if (rotated) {
            int holder;
            holder = size.height;
            size.height = size.width;
            size.width = holder;
        }

        m_image_width = size.width;
        m_image_height = size.height;

        if (m_task == null || m_task.getStatus() == AsyncTask.Status.FINISHED) {
            m_task = new CalculateStatsTask().execute(new byte[][]{data});
        }
    }

    protected class StatsHolder {
        private int[] m_histogram_values;
        private int m_average;
        private int m_median;
        private double m_standard_deviation;

        public StatsHolder(int[] histogramValues, int average, int median, double standardDeviation) {
            m_histogram_values = histogramValues;
            m_average = average;
            m_median = median;
            m_standard_deviation = standardDeviation;
        }

        public int[] getHistogramValues() { return m_histogram_values; }
        public int getAverage() { return m_average; }
        public int getMedian() { return m_median; }
        public double getStandardDeviation() { return m_standard_deviation; }
    }

    private class CalculateStatsTask extends AsyncTask<byte[], Void, StatsHolder> {
        @Override
        protected StatsHolder doInBackground(byte[]... bytes) {
            int[] argb = new int[m_image_width * m_image_height];
            int binNum = m_canvas_view.getBinNum();
            int[] histogramValues = new int[binNum];
            int mColor = m_color,
                length = argb.length,
                average,
                median,
                deviation;
            double standardDeviation;
            long sum = 0,
                sdSum = 0;

		    /* Use the appropriate YUV conversion routine to retrieve the
		     * data we actually intend to process.
		     */
            CameraData.convertYUV420SPtoARGB(argb, bytes[0], m_image_width, m_image_height);

		    /* Calculate histogram values */
            for (int color : argb) {
                int value = getColorValue(mColor, color),
                    bin = value / (255 / binNum);
                if (bin == binNum)
                    bin--;

                sum += value;
                histogramValues[value]++;
            }

            /* Calculate average */
            average = (int)(sum / length);

            /* Calculate the standard deviation */
            for (int color : argb) {
                deviation = getColorValue(mColor, color) - average;
                sdSum += deviation * deviation;
            }
            standardDeviation = Math.sqrt((double)sdSum / length);

            /* Calculate the median */
            Arrays.sort(argb);
            if (length % 2 == 0)
                median = (getColorValue(mColor, argb[length/2-1]) + getColorValue(mColor, argb[length/2])) / 2;
            else
                median = getColorValue(mColor, argb[length/2]);

            return new StatsHolder(histogramValues, average, median, standardDeviation);
        }

        @Override
        protected void onPostExecute(StatsHolder statsHolder) {
		    /* Transfer data/results to the canvas... */
            m_canvas_view.setHistogramValues(statsHolder.getHistogramValues());
            m_canvas_view.setAverage(statsHolder.getAverage());
            m_canvas_view.setMedian(statsHolder.getMedian());
            m_canvas_view.setStandardDeviation(statsHolder.getStandardDeviation());

		    /* Invalidate the canvas, forcing it to be redrawn with the new data.
		     * You can do this in other places, evaluate what makes sense to you.
		     */
            m_canvas_view.invalidate();
        }

        private int getColorValue(int color, int argb) {
            switch (color) {
                case Color.RED:
                    return Color.red(argb);
                case Color.GREEN:
                    return Color.green(argb);
                case Color.BLUE:
                    return Color.blue(argb);
                default:
                    return 0;
            }
        }
    }

    /* Accessors */
    public AsyncTask getTask() { return m_task; }

    public void setCanvasView(CanvasView canvas_view) {
        m_canvas_view = canvas_view;
    }
    public void setColor(int color) { m_color = color; }
}

