package nl.uva.multimedia.camera;
/* 
 * Framework for camera processing and visualisation
 *
 * For the Multimedia course in the BSc Computer Science 
 * at the University of Amsterdam 
 *
 * I.M.J. Kamps, S.J.R. van Schaik, R. de Vries (2013)
 */

/* XXX Yes, you should change stuff here */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SeekBar;

final class BinNumSlider extends SeekBar implements SeekBar.OnSeekBarChangeListener {
    private CanvasView m_canvas_view = null;
    private int offset = 0;

    /* Necessary constructors */
    public BinNumSlider(Context context) {
        super(context);
        setup();
    }

    public BinNumSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public BinNumSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }

    private void setup() { setOnSeekBarChangeListener(this); }

    /* This method is called when the user moves the slider. */
    public void onProgressChanged(SeekBar slider, int progress, boolean from_user) {
        /* Do something with progress (the new position of the slider) here... */
        m_canvas_view.setBinNum(getProgress() + offset);
    }

    /* Part of interface, but unused. */
    public void onStartTrackingTouch(SeekBar slider) {
    }

    public void onStopTrackingTouch(SeekBar slider) {
    }

    public void setCanvasView(CanvasView canvasView) {
        m_canvas_view = canvasView;
    }
    public void setOffset(int offset) {
        this.offset = offset;
    }
}
